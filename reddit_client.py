import os, requests, time
import datetime as dt
import praw
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.keys import Keys


class RedditClient:
    def __init__(self) -> None:
        # Generate new one if it's not valid anymore
        self._redgifs_authorization_token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwczovL3d3dy5yZWRnaWZzLmNvbS8iLCJpYXQiOjE2ODM3OTk5MTEsImV4cCI6MTY4Mzg4NjMxMSwic3ViIjoiY2xpZW50LzE4MjNjMzFmN2QzLTc0NWEtNjU4OS0wMDA1LWQ4ZThmZTBhNDRjMiIsInNjb3BlcyI6InJlYWQiLCJ2YWxpZF9hZGRyIjoiODguMTIuMTk5LjE1MSIsInZhbGlkX2FnZW50IjoiTW96aWxsYS81LjAgKFdpbmRvd3MgTlQgMTAuMDsgV2luNjQ7IHg2NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzExMy4wLjAuMCBTYWZhcmkvNTM3LjM2IEVkZy8xMTMuMC4xNzc0LjM1IiwicmF0ZSI6LTF9.YkRsm4D8t0gQzDp5O5eDl4HPseFGP61G4msv1VZ_skWYlbfPh4Zlt7geO-l2ilt9XfsmYcrX8YN2GB1COp1oODKpCmuM2kXHwzdQnByjyMB03p1GiHQUPyDlaWu_jwqKISBU-ytYkkAaa55EyirK3zSHpBmvNTH8KesEl6q_6Vt-66buT0z3tlnTs-KWgqXod-VwHqrR9gYnEZX_KpaFVk_ka0Rk1hPwd592TZPANw1wPRrnNglCD4W7SNi7OIlvZ9Jbg7Zyga97RHgtcLQ8j-bbIHI2o0lR1MJg4Ly0I1-lg5G9fp3pQXn8PwTh_2L4o3Huh6LtNIMDFJLT9U_r5g"
        self.reddit_authorized: praw.Reddit = self._create_reddit_authenticated_client()
        self.nsfw_following: list[str] = self._get_following_nsfw_usernames()

    def _create_reddit_authenticated_client(self):
        return praw.Reddit(
            client_id=os.environ["CLIENT_ID"],         # your client id
            client_secret=os.environ["CLIENT_SECRET"],      # your client secret
            user_agent=os.environ["USER_AGENT"],        # your user agent
            username=os.environ["REDDIT_USERNAME"],        # your reddit username
            password=os.environ["REDDIT_PASSWORD"]         # your reddit password
        )

    def _get_following_nsfw_usernames(self) -> list[str]:
        following = []
        for sub in self.reddit_authorized.user.subreddits(limit=None):
            if sub.display_name_prefixed.startswith("u/") and sub.over18:
                following.append(sub.display_name_prefixed[2:])

        return following
    
    def _submission_date(self, submission_created) -> str:
        return dt.datetime.fromtimestamp(int(submission_created)).strftime("%Y-%m-%dT%H-%M-%SZ")

    def get_data_user_submssions_nsfw(self, username: str):
        print(username)
        submissions_df = pd.DataFrame()
        user = self.reddit_authorized.redditor(username)

        download_directory = os.path.join(os.getcwd(), f"downloads\\{username}")
        if not os.path.exists(download_directory):
            try:
                os.mkdir(download_directory)
            except FileExistsError:
                None

        submissions_nsfw = list(filter(lambda x: x.over_18, user.submissions.new(limit=None)))#[:4]

        for submission in submissions_nsfw:
            submission_info = {"ID": [submission.id], 
                               "Author": [submission.author.name],
                               "Title": [submission.title],
                               "Subreddit": [f"r/{submission.subreddit.display_name}"], 
                               "Created_at": [dt.datetime.fromtimestamp(int(submission.created)).strftime("%Y-%m-%dT%H:%M:%SZ")],
                               "content_url": [submission.url],
                               "shortlink": [submission.shortlink],
                               "permalink": [f"https://www.reddit.com{submission.permalink}"]
                               }
            submissions_df = pd.concat(
                [
                    submissions_df,
                    pd.DataFrame(submission_info)
                ]
            )
            self.download_media_submission(submission, username, download_directory)
        
        submissions_df.to_csv(os.path.join(download_directory, f"{username}_submission_data.csv"), 
                              index=False,
                              sep=";")

    def download_media_submission(self,
                                   submission: praw.models.reddit.submission.Submission,
                                   username: str,
                                   download_directory: str):
        print(submission.id)
        if submission.removed_by_category is not None or "vidble" in submission.url or submission.id in ["fgmlzd", "dwssbs", "dwt02f", "ccqrml", "c6bz1o", "9y08cd", "9lp0sd", "9kg6zg", "9k5hib"]: #, "12cwe94", "12ciozk"
            return None
        elif "gallery" in submission.url:
            self._download_reddit_gallery(download_directory, submission, submission.url)
        elif "com/a/" in submission.url and "imgur" in submission.url:
            self._download_imgur_album(download_directory, submission, submission.url)
        elif "gfycat.com" in submission.url or "redgifs.com" in submission.url:
            self._download_from_gfycat_redgifs(download_directory, submission, submission.url)
        elif "reddit.com" in submission.url or submission.id == "jjhdho":
            self._download_submission_text(download_directory, submission, submission.url)
        elif "gif" in submission.url and "imgur" in submission.url:
            self._download_imgur_gif(download_directory, submission, submission.url)
        elif not "gif" in submission.url and "imgur" in submission.url and not "jpg" in submission.url:
            self._download_imgur_video(download_directory, submission, submission.url)
        elif "v.redd.it" in submission.url:
            response = requests.get(submission.url, timeout=5)
            with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id}.mp4"), "wb") as file:
                file.write(response.content)
        elif "erome" in submission.url:
            self._download_erome_video(download_directory, submission, submission.url)
        else:
            response = requests.get(submission.url, timeout=5)
            with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id}.{submission.url.split('.')[-1]}"), "wb") as file:
                file.write(response.content)

    def _download_reddit_gallery(self, download_directory: str, submission, url: str):
        images_urls= []
        driver = webdriver.Chrome("../chromedriver.exe")
        driver.get(submission.url)
        driver.find_element(By.CLASS_NAME, "_1dwExqTGJH2jnA-MYGkEL-").click()
        n_images = int(driver.find_element(By.CLASS_NAME, "_61i6grM3um1yuw4GrN97P").text.split("/")[-1])
        for i in range(n_images):
            driver.find_element(By.CLASS_NAME, "_38uXo2TofvhEuLKeH-rRcV").click()
        # while True:
        #     try:
        #         driver.find_element(By.CLASS_NAME, "_38uXo2TofvhEuLKeH-rRcV").click()
        #     except NoSuchElementException:
        #         break

        # web_elements = driver.find_elements(By.CLASS_NAME, "_1dwExqTGJH2jnA-MYGkEL-")
        # time.sleep(0.1)
        web_elements = driver.find_elements(By.CLASS_NAME, "_1dwExqTGJH2jnA-MYGkEL-")

        # print(len(web_elements), "-")
        for web_element in web_elements:
            # print(len(web_elements))
            images_urls.append(web_element.get_attribute("src"))

        for i, image_url in enumerate(images_urls):
            image_url = image_url.replace("preview.", "i.").split("?")[0]
            if "gif" in image_url and "imgur" in image_url:
                self._download_imgur_gif(download_directory, submission, image_url)
            
            response = requests.get(image_url, timeout=5)
            with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id} {i}.{image_url.split('.')[-1]}"), "wb") as file:
                file.write(response.content)

    def _download_imgur_gif(self, download_directory: str, submission, url: str):
        # url = url+"v"
        url = url.replace(".gif", ".mp4")
        # response = requests.get(submission.preview["reddit_video_preview"]["fallback_url"], stream=True)
        response = requests.get(url)
        with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id}.mp4"), "wb") as file:
            file.write(response.content)

    def _download_imgur_album(self, download_directory: str, submission, url: str):
        response = requests.get(url=url+"/zip")
        if response.status_code == 200:
            with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id}.mp4"), "wb") as file:
                file.write(response.content)
                return None
        else:
            driver = webdriver.Chrome("../chromedriver.exe")
            driver.get(url)
            try:
                WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, "btn-wall--yes")))
                driver.find_element(By.CLASS_NAME, "btn-wall--yes").click()
            except TimeoutException:
                pass
            try:
                content_url = driver.find_element(By.CLASS_NAME, "image-placeholder").get_attribute("src")
                filename = f"{self._submission_date(submission.created)}{submission.id}.jpg"
            except NoSuchElementException:
                content_url = driver.find_element(By.CLASS_NAME, "PostVideo-video-wrapper").find_element(By.CSS_SELECTOR, "source").get_attribute("src")
                filename = f"{self._submission_date(submission.created)}{submission.id}.mp4"
            driver.quit()
            
            response = requests.get(content_url)
            with open(os.path.join(download_directory, filename), "wb") as file:
                file.write(response.content)

        # images_urls = []
        # driver = webdriver.Chrome("../chromedriver.exe")
        # driver.get(submission.url)
        # try:
        #     # driver.find_element(By.CLASS_NAME, "Wall").click()           
        #     driver.find_element(By.CLASS_NAME, "btn-wall--yes").click()
        #     driver.find_element(By.CLASS_NAME, "Gallery-Options").click()
        #     for web_element in driver.find_elements(By.CSS_SELECTOR, "button"):
        #         try:
        #             web_element.find_element(By.XPATH, "//span[contains(text(), 'Download')]").click()

        #         except:
        #             continue
            
        #     driver.find_element(By.CLASS_NAME, "Dropdown-title").click()
        #     driver.find_element(By.TAG_NAME, "html").send_keys(Keys.END) 
        # except NoSuchElementException:
        #     pass
        
        # web_elements = driver.find_elements(By.CLASS_NAME, "image-placeholder")
        # print(len(web_elements), "-")
        # for web_element in web_elements:
        #     print(len(web_elements))
        #     images_urls.append(web_element.get_attribute("src"))

        # for i, image_url in enumerate(images_urls):
        #     response = requests.get(image_url, timeout=5)
        #     with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id} {i}.webp"), "wb") as file:
        #         file.write(response.content)

    def _download_from_gfycat_redgifs(self, download_directory: str, submission, url: str):
        driver = webdriver.Chrome("../chromedriver.exe")
        driver.get(submission.url)
        if "redgifs" in driver.current_url:
        #     WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.CLASS_NAME, "isLoaded")))
        #     video_url = driver.find_element(By.CLASS_NAME, "isLoaded").get_attribute("src")
            API_URL_REDGIFS = 'https://api.redgifs.com/v2/gifs/'
            redgifs_ID = submission.url.split('/')[-1]
            driver.quit()
            if ".jpg" in redgifs_ID:
                redgifs_ID = redgifs_ID[:-4]
                filename = f"{self._submission_date(submission.created)}{submission.id}.jpg"
            else:
                filename = f"{self._submission_date(submission.created)}{submission.id}.mp4"

            if submission.created < 1559318685.0:                    # submission.id in ["86huom", "pgx487", "bv9duw"]
                redgifs_ID = redgifs_ID.lower()

            response = requests.get(API_URL_REDGIFS + redgifs_ID, headers={"Authorization": self._redgifs_authorization_token})
            response = requests.get(response.json()['gif']['urls']['hd'])

            with open(os.path.join(download_directory, filename), "wb") as file:
                file.write(response.content)
        
        else:
            video_url = driver.find_element(By.CLASS_NAME, "video").find_element(By.TAG_NAME, "source").get_attribute("src")
            driver.quit()

            response = requests.get(video_url)
            with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id}.mp4"), "wb") as file:
                file.write(response.content)

            # session = requests.Session()
            # response = session.get(API_URL_REDGIFS + redgifs_ID, headers={})
            # # Get content.
            # video_url = response.json()['gif']['urls']['hd']
            # with session.get(video_url, stream=True) as r:
            #     with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id}.mp4"), "wb") as file:
            #         for chunk in r.iter_content(chunk_size=8192): 
            #             # If you have chunk encoded response uncomment 
            #             # if and set chunk_size parameter to None.
            #             # if chunk:
            #             file.write(chunk)

            # response = requests.get(video_url, timeout=5)
            # with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id}.mp4"), "wb") as file:
            #     file.write(response.content)

    def _download_submission_text(self, download_directory: str, submission, url: str):
            with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id}.txt"), "w", encoding="utf8") as file:
                file.write(f"{submission.title}\n\n{submission.selftext}")

    def _download_imgur_video(self, download_directory: str, submission, url: str):
        response = requests.get(submission.url+".mp4")
        with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id}.mp4"), "wb") as file:
            file.write(response.content)

    def _download_erome_video(self, download_directory: str, submission, url: str):
        driver = webdriver.Chrome("../chromedriver.exe")
        driver.get(submission.url)
        driver.find_element(By.CLASS_NAME, "enter").click()
        try:
            driver.find_element(By.CLASS_NAME, "vjs-big-play-button").click()
        except NoSuchElementException:
            driver.back()
            driver.find_element(By.CLASS_NAME, "vjs-big-play-button").click()

        video_url = driver.find_element(By.CLASS_NAME, "video").find_element(By.CSS_SELECTOR, "source").get_attribute("src")
        response = requests.get(video_url, headers={"Referer": "https://www.erome.com/"})
        with open(os.path.join(download_directory, f"{self._submission_date(submission.created)}{submission.id}.mp4"), "wb") as file:
            file.write(response.content)
